﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkuliatorius
{
    public partial class Form1 : Form
    {
        string X;
        string Operacija;

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("2");
        }

        private void button0_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "0")
            {
                return;
            }

            AtspauzdinkSkaiciu("0");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            AtspauzdinkSkaiciu("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {     
           AtspauzdinkSkaiciu("9");
        }

        public void AtspauzdinkSkaiciu(string Skaicius)
        {
            if(Ekranas.Text.Length < 8)
            {
                Ekranas.Text = Ekranas.Text + Skaicius;
            }
        }

        private void buttonkablelis_Click(object sender, EventArgs e)
        {
            if(Ekranas.Text == "")
            {
                Ekranas.Text = "0.";
             }

            if (!Ekranas.Text.Contains("."))
            {
            Ekranas.Text = Ekranas.Text + (".");
            }

          
        }

        private void buttonplius_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Text = ""; //Ekranas.Clear(); 
            Operacija = "+";
        }

        private void buttonlygu_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                return;
            }

            float x = Convert.ToSingle(X);
            float y = Convert.ToSingle(Ekranas.Text);

            float rezultatas = 0;

            if (Operacija == "+")
            {
                 rezultatas = x + y;
            }
            else if (Operacija == "-")
            {
                 rezultatas = x - y;
            }   
                  else if (Operacija == "/")
            {
                rezultatas = x / y;
            }
            else if (Operacija == "*")
            {
                rezultatas = x * y;
            }
           

            Ekranas.Text = rezultatas.ToString();
        }

        private void buttonminus_Click(object sender, EventArgs e)
        {
            {
                X = Ekranas.Text;
                Ekranas.Text = ""; //Ekranas.Clear(); 
                Operacija = "-";
            }

        }

        private void buttondalyba_Click(object sender, EventArgs e)
        {
            {
                X = Ekranas.Text;
                Ekranas.Text = ""; //Ekranas.Clear(); 
                Operacija = "/";
            }
        }

        private void buttondaugyba_Click(object sender, EventArgs e)
        {
            {
                X = Ekranas.Text;
                Ekranas.Text = ""; //Ekranas.Clear(); 
                Operacija = "*";
            }
        }

        private void buttonPliusMinus_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                return;
            }

            float y = Convert.ToSingle(Ekranas.Text);
            Ekranas.Text = (y * -1).ToString();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {

        }

        private void buttonTrinti_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                return;
            }

            Ekranas.Text = Ekranas.Text.Remove(Ekranas.Text.Length - 1);
        }

        private void buttonSaknis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                return;
            }

            float y = Convert.ToSingle(Ekranas.Text);
           
            Ekranas.Text = Math.Sqrt(y).ToString();
        }
    }
    }

