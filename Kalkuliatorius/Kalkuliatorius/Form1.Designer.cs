﻿namespace Kalkuliatorius
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttondaugyba = new System.Windows.Forms.Button();
            this.buttonminus = new System.Windows.Forms.Button();
            this.buttonplius = new System.Windows.Forms.Button();
            this.buttonlygu = new System.Windows.Forms.Button();
            this.buttonkablelis = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.buttondalyba = new System.Windows.Forms.Button();
            this.buttonPliusMinus = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonTrinti = new System.Windows.Forms.Button();
            this.buttonSaknis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ekranas.Location = new System.Drawing.Point(12, 12);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(230, 52);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Ekranas.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 179);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(53, 41);
            this.button1.TabIndex = 1;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(71, 179);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(53, 41);
            this.button2.TabIndex = 2;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(130, 179);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(53, 41);
            this.button3.TabIndex = 3;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 132);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(53, 41);
            this.button4.TabIndex = 4;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(71, 132);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(53, 41);
            this.button5.TabIndex = 5;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(130, 132);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(53, 41);
            this.button6.TabIndex = 6;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(12, 85);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(53, 41);
            this.button7.TabIndex = 7;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(71, 85);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(53, 41);
            this.button8.TabIndex = 8;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(130, 85);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(53, 41);
            this.button9.TabIndex = 9;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // buttondaugyba
            // 
            this.buttondaugyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttondaugyba.Location = new System.Drawing.Point(189, 85);
            this.buttondaugyba.Name = "buttondaugyba";
            this.buttondaugyba.Size = new System.Drawing.Size(53, 41);
            this.buttondaugyba.TabIndex = 10;
            this.buttondaugyba.Text = "*";
            this.buttondaugyba.UseVisualStyleBackColor = true;
            this.buttondaugyba.Click += new System.EventHandler(this.buttondaugyba_Click);
            // 
            // buttonminus
            // 
            this.buttonminus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonminus.Location = new System.Drawing.Point(189, 179);
            this.buttonminus.Name = "buttonminus";
            this.buttonminus.Size = new System.Drawing.Size(53, 41);
            this.buttonminus.TabIndex = 11;
            this.buttonminus.Text = "-";
            this.buttonminus.UseVisualStyleBackColor = true;
            this.buttonminus.Click += new System.EventHandler(this.buttonminus_Click);
            // 
            // buttonplius
            // 
            this.buttonplius.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonplius.Location = new System.Drawing.Point(189, 226);
            this.buttonplius.Name = "buttonplius";
            this.buttonplius.Size = new System.Drawing.Size(53, 41);
            this.buttonplius.TabIndex = 12;
            this.buttonplius.Text = "+";
            this.buttonplius.UseVisualStyleBackColor = true;
            this.buttonplius.Click += new System.EventHandler(this.buttonplius_Click);
            // 
            // buttonlygu
            // 
            this.buttonlygu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonlygu.Location = new System.Drawing.Point(130, 226);
            this.buttonlygu.Name = "buttonlygu";
            this.buttonlygu.Size = new System.Drawing.Size(53, 41);
            this.buttonlygu.TabIndex = 13;
            this.buttonlygu.Text = "=";
            this.buttonlygu.UseVisualStyleBackColor = true;
            this.buttonlygu.Click += new System.EventHandler(this.buttonlygu_Click);
            // 
            // buttonkablelis
            // 
            this.buttonkablelis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonkablelis.Location = new System.Drawing.Point(71, 226);
            this.buttonkablelis.Name = "buttonkablelis";
            this.buttonkablelis.Size = new System.Drawing.Size(53, 41);
            this.buttonkablelis.TabIndex = 14;
            this.buttonkablelis.Text = ",";
            this.buttonkablelis.UseVisualStyleBackColor = true;
            this.buttonkablelis.Click += new System.EventHandler(this.buttonkablelis_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(12, 226);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(53, 41);
            this.button0.TabIndex = 15;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // buttondalyba
            // 
            this.buttondalyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttondalyba.Location = new System.Drawing.Point(189, 132);
            this.buttondalyba.Name = "buttondalyba";
            this.buttondalyba.Size = new System.Drawing.Size(53, 41);
            this.buttondalyba.TabIndex = 16;
            this.buttondalyba.Text = "/";
            this.buttondalyba.UseVisualStyleBackColor = true;
            this.buttondalyba.Click += new System.EventHandler(this.buttondalyba_Click);
            // 
            // buttonPliusMinus
            // 
            this.buttonPliusMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPliusMinus.Location = new System.Drawing.Point(12, 275);
            this.buttonPliusMinus.Name = "buttonPliusMinus";
            this.buttonPliusMinus.Size = new System.Drawing.Size(53, 41);
            this.buttonPliusMinus.TabIndex = 17;
            this.buttonPliusMinus.Text = "+-";
            this.buttonPliusMinus.UseVisualStyleBackColor = true;
            this.buttonPliusMinus.Click += new System.EventHandler(this.buttonPliusMinus_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.Location = new System.Drawing.Point(71, 275);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(53, 41);
            this.buttonClear.TabIndex = 18;
            this.buttonClear.Text = "C";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonTrinti
            // 
            this.buttonTrinti.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTrinti.Location = new System.Drawing.Point(130, 273);
            this.buttonTrinti.Name = "buttonTrinti";
            this.buttonTrinti.Size = new System.Drawing.Size(53, 41);
            this.buttonTrinti.TabIndex = 19;
            this.buttonTrinti.Text = "⌫";
            this.buttonTrinti.UseVisualStyleBackColor = true;
            this.buttonTrinti.Click += new System.EventHandler(this.buttonTrinti_Click);
            // 
            // buttonSaknis
            // 
            this.buttonSaknis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaknis.Location = new System.Drawing.Point(189, 275);
            this.buttonSaknis.Name = "buttonSaknis";
            this.buttonSaknis.Size = new System.Drawing.Size(53, 41);
            this.buttonSaknis.TabIndex = 20;
            this.buttonSaknis.Text = "√";
            this.buttonSaknis.UseVisualStyleBackColor = true;
            this.buttonSaknis.Click += new System.EventHandler(this.buttonSaknis_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 328);
            this.Controls.Add(this.buttonSaknis);
            this.Controls.Add(this.buttonTrinti);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonPliusMinus);
            this.Controls.Add(this.buttondalyba);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.buttonkablelis);
            this.Controls.Add(this.buttonlygu);
            this.Controls.Add(this.buttonplius);
            this.Controls.Add(this.buttonminus);
            this.Controls.Add(this.buttondaugyba);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Ekranas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttondaugyba;
        private System.Windows.Forms.Button buttonminus;
        private System.Windows.Forms.Button buttonplius;
        private System.Windows.Forms.Button buttonlygu;
        private System.Windows.Forms.Button buttonkablelis;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button buttondalyba;
        private System.Windows.Forms.Button buttonPliusMinus;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonTrinti;
        private System.Windows.Forms.Button buttonSaknis;
    }
}

